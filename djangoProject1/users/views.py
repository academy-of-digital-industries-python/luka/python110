from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView

from users.forms import SignUpForm
from users.models import User, Profile


# Create your views here.
class SignInView(LoginView):
    template_name = 'auth/signin.html'
    next_page = reverse_lazy('blog:home')
    redirect_authenticated_user = True


class SignOutView(LogoutView):
    next_page = reverse_lazy('blog:home')


class SignUpView(CreateView):
    form_class = SignUpForm
    template_name = 'auth/signup.html'
    success_url = reverse_lazy('blog:home')


class ProfileView(DetailView):
    queryset = Profile.objects.all()
    template_name = 'auth/profile.html'
