from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):

    def save(self, *args, **kwargs):
        if not self.profile:
            self.profile = Profile.objects.create(user=self)
        super().save(*args, **kwargs)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def get_post_count(self) -> int:
        return self.user.post_set.count()

    def get_comment_count(self) -> int:
        return self.user.comment_set.count()
