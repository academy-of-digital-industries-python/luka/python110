from django.urls import path
from users.views import SignInView, SignOutView, SignUpView, ProfileView

app_name = 'users'

urlpatterns = [
    path('sign-in/', SignInView.as_view(), name='sign-in'),
    path('sign-out/', SignOutView.as_view(), name='sign-out'),
    path('sign-up/', SignUpView.as_view(), name='sign-up'),
    path('profile/<int:pk>/', ProfileView.as_view(), name='profile'),
]
