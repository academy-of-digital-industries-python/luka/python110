from django.urls import path

from blog.views import home_view, post_detail_view, post_create_view, PostCreateView, PostDetailView, PostListView, \
    PostDeleteView, PostUpdateView, CommentCreateView

app_name = 'blog'

urlpatterns = [
    path('', PostListView.as_view(), name='home'),
    # path('', home_view, name='home'),
    # path('create/post/', post_create_view, name='post-create'),
    path('create/post/', PostCreateView.as_view(), name='post-create'),
    path('posts/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('posts/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('posts/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('comment/add/', CommentCreateView.as_view(), name='comment-create'),
]
