from django.contrib import admin

from blog.models import Post, Comment, Tag

# Register your models here.
admin.site.register([Post, Comment, Tag])
