from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.http import HttpRequest, HttpResponse, Http404
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from blog.models import Post, Comment
from blog.forms import PostCreateForm


# Create your views here.
def home_view(request: HttpRequest) -> HttpResponse:
    q: str = request.GET.get('q', '')
    print(f'User requested posts with title of {q}')

    return render(
        request=request,
        template_name='home.html',
        context={
            'title': 'Glory to Ukraine',
            'posts': Post.objects.order_by('-create_time').filter(title__icontains=q)
        }
    )


def post_detail_view(request: HttpRequest, pk: int) -> HttpResponse:
    # try:
    #     post: Post = Post.objects.get(pk=pk)
    # except Post.DoesNotExist:
    #     raise Http404('Request post does not exist!')
    # else:
    #     return HttpResponse(f'You chose post: {post}')

    # post: Post = get_object_or_404(Post, pk=pk)
    # return HttpResponse(f'You chose post: {post}')
    return render(request, 'post_detail.html', {
        'post': get_object_or_404(Post, pk=pk)
    })


def post_create_view(request: HttpRequest) -> HttpResponse:
    if request.method == 'GET':
        """ 
        მომხმარებელს პოსტის შექმნა უნდა 
        მივეცით ცარიელი ფორმა რაც უნდა შეავსოს 
        """
        return render(request, 'post_create.html', {
            'form': PostCreateForm()
        })

    # print(request.POST)
    # # if request.POST
    # print(request.GET)
    # print(Post.objects.create(
    #     title=request.POST['title'],
    #     author=request.POST['author'],
    #     text=request.POST['text'],
    # ))
    """
    მომხმარებელმა ჩაგვაბარა ფორმა
    """
    form = PostCreateForm(request.POST)
    if form.is_valid():
        """
        ყველაფერი სწორია და ვუბრუნებთ მომხმარებელს წარმატებულ პასუხს
        """
        post: Post = form.save()
        return redirect('blog:post-detail', pk=post.pk)
    """
    მომხმარებელმა სადღაც შეცდომა დაუშვა
    ვუბრუნებთ დატაც რაც უკვე შეავსო და ვეუბნებით სადაა შეცდომა
    """
    return render(request, 'post_create.html', {
        'form': form
    })


class PostCreateView(LoginRequiredMixin, CreateView):
    queryset = Post.objects.all()
    form_class = PostCreateForm
    template_name = 'post_create.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('blog:post-detail', kwargs={'pk': self.object.pk})


class PostDetailView(DetailView):
    template_name = 'post_detail.html'
    queryset = Post.objects.all()


class PostListView(ListView):
    template_name = 'home.html'
    context_object_name = 'posts'
    paginate_by = 5

    def get_queryset(self):
        return Post.objects.filter(title__icontains=self.request.GET.get('q', '')).order_by('-create_time')


class PostDeleteView(LoginRequiredMixin, DeleteView):
    success_url = reverse_lazy('blog:home')
    template_name = 'post_delete.html'
    queryset = Post.objects.all()

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Post.objects.all()

        return Post.objects.filter(author=user)


class PostUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'post_update.html'
    fields = ('title', 'text')

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Post.objects.all()

        return Post.objects.filter(author=user)

class CommentCreateView(LoginRequiredMixin, CreateView):
    http_method_names = ['post']
    queryset = Comment.objects.all()
    fields = ['post', 'text']
    template_name = 'post_create.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('blog:post-detail', kwargs={'pk': self.object.post.pk})
